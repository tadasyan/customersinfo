
import DAO.CustomerPaymentDao;
import DAO.CustomersDao;
import DAO.implementation.CustomerPaymentDaoImpl;
import DAO.implementation.CustomersDaoImpl;
import com.mycompany.customersinfo.CustomerPayment;
import com.mycompany.customersinfo.Customers;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alexey
 */
public class Test {
    public static void main(String[] args) throws SQLException {
        CustomersDao customerPaymentDao = new CustomersDaoImpl();
        List<Customers> cpd =  customerPaymentDao.getAllCustomers();
        for (Customers i: cpd){
            System.out.println(i.getName() + " " + i.getLastname() + " -  total amount: " + i.getTotalAmount());
        }
    }
}
