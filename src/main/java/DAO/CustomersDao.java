/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import com.mycompany.customersinfo.Customers;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Alexey
 */
public interface CustomersDao {
    
    public List getAllCustomers();
    public void addCustomer(Customers customers);
    
    
}
