/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO.implementation;

import DAO.CustomersDao;
import com.mycompany.customersinfo.Customers;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
//import javax.swing.JOptionPane;
import org.hibernate.Session;
import util.HibernateUtil;

/**
 *
 * @author Alexey
 */
public class CustomersDaoImpl implements CustomersDao {
    
    @Override
    public void addCustomer(Customers customers){
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(customers);
            session.getTransaction().commit();
        } catch (Exception e){
            //JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
            System.out.println("Ошибка I/O");
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
    } 

    @Override
    //@SuppressWarnings("NULL POINTER EXEPTION")
    public List<Customers> getAllCustomers(){
        Session session = null;
        List<Customers> custs = new ArrayList();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            //может вернуть null, если таблица пустая
            custs = session.createCriteria(Customers.class).list();
        } catch (Exception e){
            System.out.println("ошибка в getAllCustomers");
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
        return custs;
    }
    
}
