/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO.implementation;

import DAO.CustomerPaymentDao;
import com.mycompany.customersinfo.CustomerPayment;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import util.HibernateUtil;

/**
 *
 * @author Alexey
 */
public class CustomerPaymentDaoImpl implements CustomerPaymentDao {

    @Override
    public List<Object[]> getCertainCustomerPayment(String name, String lastName){
        Session session = null;
        List<Object[]> cp = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(CustomerPayment.class);
            criteria.createAlias("customers", "customers", JoinType.INNER_JOIN);
            criteria.add(Restrictions.eq("customers.name", name))
                    .add(Restrictions.eq("customers.lastname", lastName));
            criteria.setProjection(Projections.projectionList()
                    .add(Projections.property("date"))
                    .add(Projections.property("amount")));
            cp = criteria.list();
            }catch(Exception e){
            System.out.println(e);
        }finally{
            if (session != null && session.isOpen())
                session.close();
            return cp;
        }
    }

    @Override
    public void addPayment(CustomerPayment customerPayment){
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(customerPayment);
            session.getTransaction().commit();
        } catch (Exception e){
            //JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
            System.out.println("Ошибка I/O");
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
    }

    @Override
    public List<Object[]> getReport(Date dateMin, Date dateMax) {
        Session session = null;
        List<Object[]> cp = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(CustomerPayment.class);
            criteria.createAlias("customers", "customers", JoinType.INNER_JOIN);
//            criteria.add(Restrictions.ge("date", dateMin))
//                    .add(Restrictions.le("date", dateMax));
            criteria.add(Restrictions.between("date", dateMin, dateMax));
            criteria.setProjection(Projections.projectionList()
                    .add(Projections.groupProperty("customers.id"))
                    .add(Projections.property("customers.name"))
                    .add(Projections.sum("amount")));
            
            cp = criteria.list();
        }catch(Exception e){
            System.out.println(e);
        }finally{
            if (session != null && session.isOpen())
                session.close();
            return cp;
        }
    }
    
}
