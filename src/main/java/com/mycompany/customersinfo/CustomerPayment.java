package com.mycompany.customersinfo;
// Generated Aug 3, 2014 11:31:43 AM by Hibernate Tools 3.6.0


import java.util.Date;

/**
 * Customerpayment generated by hbm2java
 */
public class CustomerPayment  implements java.io.Serializable {


     private int id;
     private Customers customers;
     private Double amount;
     private Date date;

    public CustomerPayment() {
    }

	
    public CustomerPayment(int id, Date date) {
        this.id = id;
        this.date = date;
    }
    public CustomerPayment(int id, Customers customers, Double amount, Date date) {
       this.id = id;
       this.customers = customers;
       this.amount = amount;
       this.date = date;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Customers getCustomers() {
        return this.customers;
    }
    
    public void setCustomers(Customers customers) {
        this.customers = customers;
    }
    public Double getAmount() {
        return this.amount;
    }
    
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CustomerPayment{" + "id=" + id + ", customers=" + customers + ", amount=" + amount + ", date=" + date + '}';
    }
    
}


