<%-- 
    Document   : test
    Created on : Aug 6, 2014, 6:21:27 PM
    Author     : Alexey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List of customers</title>
    </head>
    <body>
        <%-->
        <table border="1">           
            <tr>
                <td>${requestScope['customersList'].name}</td> 
                <td>${requestScope['customersList'].lastname}</td> 
                <td>${requestScope['cl'].totalAmount}</td>
            </tr>              
        </table>
        <--%>
       
        <table border="1">
            <tr>
                <td> Name </td>
                <td> Lastname </td>
                <td> Total amount </td>
            </tr>
            <c:forEach var="element" items="${customersList}">
                <tr>
                    <td> ${element.name} </td> 
                    <td> ${element.lastname} </td> 
                    <td> ${element.totalAmount} </td>
                </tr> 
             </c:forEach>
        </table>
        
    </body>
</html>
